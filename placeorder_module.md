# Title: Place Order

## Pre-Checklist

- I have searched for, and did not find, an existing issue or other requirement in the LibreFoodPantry. - Done

## Stories

1. As a Student, I want Place a Custom Order so that I can order what I want.
2. As a Student, I want Place a Generic Order so that I can order the necessities.
3. As a Student, I want a place to enter my allergies so that I don&#39;t get injured while eating my food.
4. As a Student, I want a place to indicate my religious preferences so I don&#39;t eat outside of them.
5. As a Student, I want an email confirmation if my order was successfully placed so that I can feel confident my order was received.
6. As a Student, I want to be aware of items that are not available so that I don&#39;t request items that I can&#39;t have.

## Ready Checklist

| Story 1: As a Student, I want Place a Custom Order so that I can order what I want.                                         |      |
| --------------------------------------------------------------------------------------------------------------------------- | ---- |
| Independent of other issues being worked on                                                                                 | Done |
| Negotiable (and negotiated)                                                                                                 | Done |
| Valuable (the value to an identified role has been identified)                                                              | Done |
| Estimable (the size of this story has been estimated)                                                                       | Done |
| Small (can be completed in 50% or less of a single iteration)                                                               | Done |
| Testable (has testable acceptance criteria)                                                                                 | Done |
| The roles that benefit from this issue are labeled (e.g., role:\*).                                                         | Done |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map. | Done |

| Story 2: As a Student, I want Place a Generic Order so that I can order the necessities.                                    |      |
| --------------------------------------------------------------------------------------------------------------------------- | ---- |
| Independent of other issues being worked on                                                                                 | Done |
| Negotiable (and negotiated)                                                                                                 | Done |
| Valuable (the value to an identified role has been identified)                                                              | Done |
| Estimable (the size of this story has been estimated)                                                                       | Done |
| Small (can be completed in 50% or less of a single iteration)                                                               | Done |
| Testable (has testable acceptance criteria)                                                                                 | Done |
| The roles that benefit from this issue are labeled (e.g., role:\*).                                                         | Done |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map. | Done |

| Story 3: As a Student, I want a place to enter my allergies so that I don’t get injured while eating my food.               |      |
| --------------------------------------------------------------------------------------------------------------------------- | ---- |
| Independent of other issues being worked on                                                                                 | Done |
| Negotiable (and negotiated)                                                                                                 | Done |
| Valuable (the value to an identified role has been identified)                                                              | Done |
| Estimable (the size of this story has been estimated)                                                                       | Done |
| Small (can be completed in 50% or less of a single iteration)                                                               | Done |
| Testable (has testable acceptance criteria)                                                                                 | Done |
| The roles that benefit from this issue are labeled (e.g., role:\*).                                                         | Done |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map. | Done |

| Story 4: As a Student, I want a place to indicate my religious preferences so I don’t eat outside of them.                  |      |
| --------------------------------------------------------------------------------------------------------------------------- | ---- |
| Independent of other issues being worked on                                                                                 | Done |
| Negotiable (and negotiated)                                                                                                 | Done |
| Valuable (the value to an identified role has been identified)                                                              | Done |
| Estimable (the size of this story has been estimated)                                                                       | Done |
| Small (can be completed in 50% or less of a single iteration)                                                               | Done |
| Testable (has testable acceptance criteria)                                                                                 | Done |
| The roles that benefit from this issue are labeled (e.g., role:\*).                                                         | Done |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map. | Done |

| Story 5: As a Student, I want an email confirmation if my order was successfully placed so that I can feel confident my order was received. |          |
| ------------------------------------------------------------------------------------------------------------------------------------------- | -------- |
| Independent of other issues being worked on                                                                                                 | Done     |
| Negotiable (and negotiated)                                                                                                                 | Not Done |
| Valuable (the value to an identified role has been identified)                                                                              | Done     |
| Estimable (the size of this story has been estimated)                                                                                       | Not Done |
| Small (can be completed in 50% or less of a single iteration)                                                                               | Done     |
| Testable (has testable acceptance criteria)                                                                                                 | Done     |
| The roles that benefit from this issue are labeled (e.g., role:\*).                                                                         | Done     |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map.                 | Not Done |

| Story 6: As a Student, I want to be aware of items that are not available so that I don’t request items that I can’t have.  |          |
|-----------------------------------------------------------------------------------------------------------------------------|----------|
| Independent of other issues being worked on                                                                                 | Done     |
| Negotiable (and negotiated)                                                                                                 | Not Done |
| Valuable (the value to an identified role has been identified)                                                              | Done     |
| Estimable (the size of this story has been estimated)                                                                       | Not Done |
| Small (can be completed in 50% or less of a single iteration)                                                               | Not Done |
| Testable (has testable acceptance criteria)                                                                                 | Not Done |
| The roles that benefit from this issue are labeled (e.g., role:*).                                                          | Done     |
| The related activity in the story map has been identified (e.g., activity:*). This should be an activity in the Story Map.  | Not Done |

## Diagrams

- Diagram D0: placeOrder module overview

![](RackMultipart20201022-4-auqgld_html_abff8d0d0101f429.png)

- Diagram D1: Order specification GUI
  ![](RackMultipart20201022-4-auqgld_html_f2573de048b11554.png)

- Diagram D2: Item Selection GUI

![](RackMultipart20201022-4-auqgld_html_95fd47e1d99ba00a.png)

- Diagram D3: Dietary Restriction Textbox

![](RackMultipart20201022-4-auqgld_html_2d203639ebdbe254.png)

- Diagram D4: Preferences Textbox

![](RackMultipart20201022-4-auqgld_html_2a113111c0b85e80.png)

- Diagram D5: Email Confirmation Textboxes

![](RackMultipart20201022-4-auqgld_html_4cc9b86a7bf9ccf3.png)

- Diagram D6: Confirmation button

![](RackMultipart20201022-4-auqgld_html_73043798cd5eaa27.png)

## Acceptance Criteria

### Scenario : The student orders a Golden Bear Order

Given : The student checks Golden Bear Order

When : The student presses submit

Then : The student is sent a confirmation email on when the order will be ready to be picked up.

### Scenario: The student orders Bread and Cereal

Given: The student checks custom order and checks bread and cereal

When: The student presses submit

Then: The student is sent a confirmation email on when the order will be ready to be picked up.

### Scenario : The student enters a non-valid wne email

Given : The student enters a non valid wne email

When : The student presses submit.

Then : An error message displays showing they cannot place an order with the email they just entered.

### Scenario : The student has allergies and wants those taken into consideration into the order.

Given : The student wants a Golden Bear Order and has some food allergy.

When : The student fills out the place order form to indicate their allergy then places their order

Then : The order is fulfilled, omitting anything the student&#39;s allergies would effect

### Scenario : The student wants to know what items are available.

Given : The student wants a custom order.

When : The student goes to click on a specific item when it&#39;s out of stock.

Then : The item checkbox is disabled.

## Related Issues

_Links to issues related to this issue, and how they are related. Note that these should use GitLab syntax for referencing issues, merge requests, milestones, etc. You may copy and paste the URL to these things, and GitLab will shorten it for you._

- Parent: [the-refinery-forge#6](https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/placeorder/the-refinery-forge/-/issues/6) This issue is a task of verifying guest&#39;s email to make sure it is valid when placing an order
- Task: [plan#5](https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/placeorder/plan/-/issues/5) The problem why the parent issues exists is because of how the previous team does not research carefully about JavaScript when implementing it, which leads to the error
- Depends-on: [plan#36](https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/placeorder/plan/-/issues/36) The issue of verifying email&#39;s guest is one of the back-end jobs, and Nodes is being implemented to handle that.
- Reference, delete when submiting: [https://gitlab.com/LibreFoodPantry/BEAR-Necessities-Market/-/issues/152](https://gitlab.com/LibreFoodPantry/BEAR-Necessities-Market/-/issues/152)

## Estimate

Story 1: As a Student, I want Place a Custom Order so that I can order what I want.

_Estimate: 5_

Story 2: As a Student, I want Place a Generic Order so that I can order the necessities.

_Estimate: 5_

Story 3: As a Student, I want a place to enter my allergies so that I don&#39;t get injured while eating my food.

_Estimate: 3_

Story 4: As a Student, I want a place to indicate my religious preferences so I don&#39;t eat outside of them.

_Estimate: 3_

Story 5: As a Student, I want an email confirmation if my order was successfully placed so that I can feel confident my order was received.

_Estimate: 2_

Story 6: As a Student, I want to be aware of items that are not available so that I don&#39;t request items that I can&#39;t have.

_Estimate: 13_
